/* Generated by Nim Compiler v1.6.10 */
#define NIM_INTBITS 32
#define NIM_EmulateOverflowChecks

#include "nimbase.h"
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
#define nimfr_(x, y)
#define nimln_(x, y)
typedef struct tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg;
typedef struct tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g;
typedef struct NimStringDesc NimStringDesc;
typedef struct TGenericSeq TGenericSeq;
typedef struct tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw;
typedef struct TNimType TNimType;
typedef struct TNimNode TNimNode;
typedef struct tyObject_TType__facALICuu8zUj0hjvbTLFg tyObject_TType__facALICuu8zUj0hjvbTLFg;
typedef struct tyObject_TLineInfo__WGx4rAexNXnczy1Avn646Q tyObject_TLineInfo__WGx4rAexNXnczy1Avn646Q;
typedef struct tyObject_TSym__nnJKJFyjDGi5mnv8jwfTGQ tyObject_TSym__nnJKJFyjDGi5mnv8jwfTGQ;
typedef struct tyObject_TIdent__1LTsGP7bSxg45u9aHcxh6OA tyObject_TIdent__1LTsGP7bSxg45u9aHcxh6OA;
typedef struct tySequence__ehmV9bTklH2Gt9cXHV9c0HLeQ tySequence__ehmV9bTklH2Gt9cXHV9c0HLeQ;
typedef struct tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g;
typedef struct tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w;
typedef struct tyObject_GcHeap__1TRH1TZMaVZTnLNcIHuNFQ tyObject_GcHeap__1TRH1TZMaVZTnLNcIHuNFQ;
typedef struct tyObject_GcStack__7fytPA5bBsob6See21YMRA tyObject_GcStack__7fytPA5bBsob6See21YMRA;
typedef struct tyObject_MemRegion__x81NhDv59b8ercDZ9bi85jyg tyObject_MemRegion__x81NhDv59b8ercDZ9bi85jyg;
typedef struct tyObject_SmallChunk__tXn60W2f8h3jgAYdEmy5NQ tyObject_SmallChunk__tXn60W2f8h3jgAYdEmy5NQ;
typedef struct tyObject_BigChunk__Rv9c70Uhp2TytkX7eH78qEg tyObject_BigChunk__Rv9c70Uhp2TytkX7eH78qEg;
typedef struct tyObject_LLChunk__XsENErzHIZV9bhvyJx56wGw tyObject_LLChunk__XsENErzHIZV9bhvyJx56wGw;
typedef struct tyObject_IntSet__EZObFrE3NC9bIb3YMkY9crZA tyObject_IntSet__EZObFrE3NC9bIb3YMkY9crZA;
typedef struct tyObject_Trunk__W0r8S0Y3UGke6T9bIUWnnuw tyObject_Trunk__W0r8S0Y3UGke6T9bIUWnnuw;
typedef struct tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw;
typedef struct tyObject_HeapLinks__PDV1HBZ8CQSQJC9aOBFNRSg tyObject_HeapLinks__PDV1HBZ8CQSQJC9aOBFNRSg;
typedef struct tyTuple__ujsjpB2O9cjj3uDHsXbnSzg tyTuple__ujsjpB2O9cjj3uDHsXbnSzg;
typedef struct tyObject_GcStat__0RwLoVBHZPfUAcLczmfQAg tyObject_GcStat__0RwLoVBHZPfUAcLczmfQAg;
typedef struct tyObject_CellSet__jG87P0AI9aZtss9ccTYBIISQ tyObject_CellSet__jG87P0AI9aZtss9ccTYBIISQ;
typedef struct tyObject_PageDesc__fublkgIY4LG3mT51LU2WHg tyObject_PageDesc__fublkgIY4LG3mT51LU2WHg;
typedef struct tyTuple__9aIi6GdTSD27YtPkWxMqNxA tyTuple__9aIi6GdTSD27YtPkWxMqNxA;
struct tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg {
tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* root;
NI entries;
};
struct TGenericSeq {
NI len;
NI reserved;
};
struct NimStringDesc {
  TGenericSeq Sup;
NIM_CHAR data[SEQ_DECL_SIZE];
};
typedef NimStringDesc* tyArray__9aijZrM1syqzleJpcL8bUyw[512];
typedef tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* tyArray__VznXOmL540VWlOp56dB9cqA[512];
typedef tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* tyArray__jSFVQ48g222vuMjoJArs8A[512];
struct tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g {
NI entries;
tyArray__9aijZrM1syqzleJpcL8bUyw keys;
NIM_BOOL isInternal;
union{
struct {tyArray__VznXOmL540VWlOp56dB9cqA vals;
} _isInternal_1;
struct {tyArray__jSFVQ48g222vuMjoJArs8A links;
} _isInternal_2;
};
};
typedef NU8 tyEnum_TNimKind__jIBKr1ejBgsfM33Kxw4j7A;
typedef NU8 tySet_tyEnum_TNimTypeFlag__v8QUszD1sWlSIWZz7mC4bQ;
typedef N_NIMCALL_PTR(void, tyProc__ojoeKfW4VYIm36I9cpDTQIg) (void* p, NI op);
typedef N_NIMCALL_PTR(void*, tyProc__WSm2xU5ARYv9aAR4l0z9c9auQ) (void* p);
struct TNimType {
NI size;
NI align;
tyEnum_TNimKind__jIBKr1ejBgsfM33Kxw4j7A kind;
tySet_tyEnum_TNimTypeFlag__v8QUszD1sWlSIWZz7mC4bQ flags;
TNimType* base;
TNimNode* node;
void* finalizer;
tyProc__ojoeKfW4VYIm36I9cpDTQIg marker;
tyProc__WSm2xU5ARYv9aAR4l0z9c9auQ deepcopy;
};
typedef NU8 tyEnum_TNimNodeKind__unfNsxrcATrufDZmpBq4HQ;
struct TNimNode {
tyEnum_TNimNodeKind__unfNsxrcATrufDZmpBq4HQ kind;
NI offset;
TNimType* typ;
NCSTRING name;
NI len;
TNimNode** sons;
};
struct tyObject_TLineInfo__WGx4rAexNXnczy1Avn646Q {
NU16 line;
NI16 col;
NI32 fileIndex;
};
typedef NU32 tySet_tyEnum_TNodeFlag__jyh9acXHkhZANSSvPIY7ZLg;
typedef NU8 tyEnum_TNodeKind__G4E4Gxe7oI2Cm03rkiOzQw;
struct tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw {
tyObject_TType__facALICuu8zUj0hjvbTLFg* typ;
tyObject_TLineInfo__WGx4rAexNXnczy1Avn646Q info;
tySet_tyEnum_TNodeFlag__jyh9acXHkhZANSSvPIY7ZLg flags;
tyEnum_TNodeKind__G4E4Gxe7oI2Cm03rkiOzQw kind;
union{
struct {NI64 intVal;
} _kind_1;
struct {NF floatVal;
} _kind_2;
struct {NimStringDesc* strVal;
} _kind_3;
struct {tyObject_TSym__nnJKJFyjDGi5mnv8jwfTGQ* sym;
} _kind_4;
struct {tyObject_TIdent__1LTsGP7bSxg45u9aHcxh6OA* ident;
} _kind_5;
struct {tySequence__ehmV9bTklH2Gt9cXHV9c0HLeQ* sons;
} _kind_6;
};
};
struct tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g {
NI refcount;
TNimType* typ;
};
struct tyObject_GcStack__7fytPA5bBsob6See21YMRA {
void* bottom;
};
struct tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w {
NI len;
NI cap;
tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g** d;
};
typedef tyObject_SmallChunk__tXn60W2f8h3jgAYdEmy5NQ* tyArray__SPr7N6UKfuF549bNPiUvSRw[256];
typedef NU32 tyArray__BHbOSqU1t9b3Gt7K2c6fQig[24];
typedef tyObject_BigChunk__Rv9c70Uhp2TytkX7eH78qEg* tyArray__N1u1nqOgmuJN9cSZrnMHgOQ[32];
typedef tyArray__N1u1nqOgmuJN9cSZrnMHgOQ tyArray__B6durA4ZCi1xjJvRtyYxMg[24];
typedef tyObject_Trunk__W0r8S0Y3UGke6T9bIUWnnuw* tyArray__lh2A89ahMmYg9bCmpVaplLbA[256];
struct tyObject_IntSet__EZObFrE3NC9bIb3YMkY9crZA {
tyArray__lh2A89ahMmYg9bCmpVaplLbA data;
};
typedef tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw* tyArray__0aOLqZchNi8nWtMTi8ND8w[2];
struct tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw {
tyArray__0aOLqZchNi8nWtMTi8ND8w link;
NI key;
NI upperBound;
NI level;
};
struct tyTuple__ujsjpB2O9cjj3uDHsXbnSzg {
tyObject_BigChunk__Rv9c70Uhp2TytkX7eH78qEg* Field0;
NI Field1;
};
typedef tyTuple__ujsjpB2O9cjj3uDHsXbnSzg tyArray__LzOv2eCDGiceMKQstCLmhw[30];
struct tyObject_HeapLinks__PDV1HBZ8CQSQJC9aOBFNRSg {
NI len;
tyArray__LzOv2eCDGiceMKQstCLmhw chunks;
tyObject_HeapLinks__PDV1HBZ8CQSQJC9aOBFNRSg* next;
};
struct tyObject_MemRegion__x81NhDv59b8ercDZ9bi85jyg {
NI minLargeObj;
NI maxLargeObj;
tyArray__SPr7N6UKfuF549bNPiUvSRw freeSmallChunks;
NU32 flBitmap;
tyArray__BHbOSqU1t9b3Gt7K2c6fQig slBitmap;
tyArray__B6durA4ZCi1xjJvRtyYxMg matrix;
tyObject_LLChunk__XsENErzHIZV9bhvyJx56wGw* llmem;
NI currMem;
NI maxMem;
NI freeMem;
NI occ;
NI lastSize;
tyObject_IntSet__EZObFrE3NC9bIb3YMkY9crZA chunkStarts;
tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw* root;
tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw* deleted;
tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw* last;
tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw* freeAvlNodes;
NIM_BOOL locked;
NIM_BOOL blockChunkSizeIncrease;
NI nextChunkSize;
tyObject_AvlNode__IaqjtwKhxLEpvDS9bct9blEw bottomData;
tyObject_HeapLinks__PDV1HBZ8CQSQJC9aOBFNRSg heapLinks;
};
struct tyObject_GcStat__0RwLoVBHZPfUAcLczmfQAg {
NI stackScans;
NI cycleCollections;
NI maxThreshold;
NI maxStackSize;
NI maxStackCells;
NI cycleTableSize;
NI64 maxPause;
};
struct tyObject_CellSet__jG87P0AI9aZtss9ccTYBIISQ {
NI counter;
NI max;
tyObject_PageDesc__fublkgIY4LG3mT51LU2WHg* head;
tyObject_PageDesc__fublkgIY4LG3mT51LU2WHg** data;
};
struct tyObject_GcHeap__1TRH1TZMaVZTnLNcIHuNFQ {
tyObject_GcStack__7fytPA5bBsob6See21YMRA stack;
NI cycleThreshold;
NI zctThreshold;
tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w zct;
tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w decStack;
tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w tempStack;
NI recGcLock;
tyObject_MemRegion__x81NhDv59b8ercDZ9bi85jyg region;
tyObject_GcStat__0RwLoVBHZPfUAcLczmfQAg stat;
tyObject_CellSet__jG87P0AI9aZtss9ccTYBIISQ marked;
tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w additionalRoots;
NI gcThreadId;
};
struct tyTuple__9aIi6GdTSD27YtPkWxMqNxA {
NimStringDesc* Field0;
tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* Field1;
NI Field2;
};
struct tySequence__ehmV9bTklH2Gt9cXHV9c0HLeQ {
  TGenericSeq Sup;
  tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* data[SEQ_DECL_SIZE];
};
N_LIB_PRIVATE N_NIMCALL(void, unsureAsgnRef)(void** dest, void* src);
N_LIB_PRIVATE N_NOINLINE(void*, newObj)(TNimType* typ, NI size);
N_LIB_PRIVATE N_NIMCALL(NI, cmp__system_1549)(NimStringDesc* x, NimStringDesc* y);
N_LIB_PRIVATE N_NIMCALL(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*, insert__vm_10029)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* h, NimStringDesc* key, tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* val);
N_LIB_PRIVATE N_NIMCALL(NimStringDesc*, copyString)(NimStringDesc* src);
static N_INLINE(void, asgnRef)(void** dest, void* src);
static N_INLINE(void, incRef__system_5329)(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c);
static N_INLINE(NI, pluspercent___system_696)(NI x, NI y);
static N_INLINE(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*, usrToCell__system_5291)(void* usr);
static N_INLINE(NI, minuspercent___system_716)(NI x, NI y);
static N_INLINE(void, decRef__system_5336)(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c);
static N_INLINE(NIM_BOOL, ltpercent___system_1005)(NI x, NI y);
static N_INLINE(void, rtlAddZCT__system_5334)(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c);
N_LIB_PRIVATE N_NOINLINE(void, addZCT__system_5285)(tyObject_CellSeq__Axo1XVm9aaQueTOldv8le5w* s, tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c);
N_LIB_PRIVATE N_NIMCALL(NimStringDesc*, copyStringRC1)(NimStringDesc* src);
static N_INLINE(void, nimGCunrefNoCycle)(void* p);
N_LIB_PRIVATE N_NIMCALL(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*, split__vm_10863)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* h);
N_LIB_PRIVATE N_NIMCALL(void, copyHalf__vm_10880)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* h, tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* result);
N_LIB_PRIVATE N_NIMCALL(NI, countSubTree__vm_12621)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* it);
extern TNimType NTInode__tPE9ckDbaXj82MdUy0JgA7w_;
extern tyObject_GcHeap__1TRH1TZMaVZTnLNcIHuNFQ gch__system_5238;
N_LIB_PRIVATE N_NIMCALL(void, initBTree__vm_8362)(tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg* Result) {
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* T1_;
	unsureAsgnRef((void**)&(*Result).root, NIM_NIL);
	(*Result).entries = 0;
	T1_ = NIM_NIL;
	T1_ = (tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) newObj((&NTInode__tPE9ckDbaXj82MdUy0JgA7w_), sizeof(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g));
	(*T1_).entries = ((NI) 0);
	(*T1_).isInternal = NIM_FALSE;
	unsureAsgnRef((void**) (&(*Result).root), T1_);
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, contains__vm_9729)(tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg b, NimStringDesc* key) {
	NIM_BOOL result;
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* x;
{	result = (NIM_BOOL)0;
	x = b.root;
	{
		while (1) {
			if (!(*x).isInternal) goto LA2;
			{
				NI j;
				NI i;
				j = (NI)0;
				i = ((NI) 0);
				{
					while (1) {
						if (!(i < (*x).entries)) goto LA5;
						j = i;
						{
							NIM_BOOL T8_;
							NI T10_;
							T8_ = (NIM_BOOL)0;
							T8_ = ((NI)(j + ((NI) 1)) == (*x).entries);
							if (T8_) goto LA9_;
							T10_ = (NI)0;
							T10_ = cmp__system_1549(key, (*x).keys[((NI)(j + ((NI) 1)))- 0]);
							T8_ = (T10_ < ((NI) 0));
							LA9_: ;
							if (!T8_) goto LA11_;
							x = (*x)._isInternal_2.links[(j)- 0];
							goto LA3;
						}
						LA11_: ;
						i += ((NI) 1);
					} LA5: ;
				}
			} LA3: ;
		} LA2: ;
	}
	{
		NI j_2;
		NI i_2;
		j_2 = (NI)0;
		i_2 = ((NI) 0);
		{
			while (1) {
				if (!(i_2 < (*x).entries)) goto LA15;
				j_2 = i_2;
				{
					NI T18_;
					T18_ = (NI)0;
					T18_ = cmp__system_1549(key, (*x).keys[(j_2)- 0]);
					if (!(T18_ == ((NI) 0))) goto LA19_;
					result = NIM_TRUE;
					goto BeforeRet_;
				}
				LA19_: ;
				i_2 += ((NI) 1);
			} LA15: ;
		}
	}
	result = NIM_FALSE;
	goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NI, pluspercent___system_696)(NI x, NI y) {
	NI result;
	result = (NI)0;
	result = ((NI) ((NU)((NU32)(((NU) (x))) + (NU32)(((NU) (y))))));
	return result;
}
static N_INLINE(void, incRef__system_5329)(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c) {
	(*c).refcount = pluspercent___system_696((*c).refcount, ((NI) 8));
}
static N_INLINE(NI, minuspercent___system_716)(NI x, NI y) {
	NI result;
	result = (NI)0;
	result = ((NI) ((NU)((NU32)(((NU) (x))) - (NU32)(((NU) (y))))));
	return result;
}
static N_INLINE(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*, usrToCell__system_5291)(void* usr) {
	tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* result;
	NI T1_;
	result = (tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*)0;
	T1_ = (NI)0;
	T1_ = minuspercent___system_716(((NI) (ptrdiff_t) (usr)), ((NI) 8));
	result = ((tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*) (T1_));
	return result;
}
static N_INLINE(NIM_BOOL, ltpercent___system_1005)(NI x, NI y) {
	NIM_BOOL result;
	result = (NIM_BOOL)0;
	result = ((NU32)(((NU) (x))) < (NU32)(((NU) (y))));
	return result;
}
static N_INLINE(void, rtlAddZCT__system_5334)(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c) {
	addZCT__system_5285((&gch__system_5238.zct), c);
}
static N_INLINE(void, decRef__system_5336)(tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* c) {
	(*c).refcount = minuspercent___system_716((*c).refcount, ((NI) 8));
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = ltpercent___system_1005((*c).refcount, ((NI) 8));
		if (!T3_) goto LA4_;
		rtlAddZCT__system_5334(c);
	}
	LA4_: ;
}
static N_INLINE(void, asgnRef)(void** dest, void* src) {
	{
		tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* T5_;
		if (!!((src == NIM_NIL))) goto LA3_;
		T5_ = (tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*)0;
		T5_ = usrToCell__system_5291(src);
		incRef__system_5329(T5_);
	}
	LA3_: ;
	{
		tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* T10_;
		if (!!(((*dest) == NIM_NIL))) goto LA8_;
		T10_ = (tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*)0;
		T10_ = usrToCell__system_5291((*dest));
		decRef__system_5336(T10_);
	}
	LA8_: ;
	(*dest) = src;
}
static N_INLINE(void, nimGCunrefNoCycle)(void* p) {
	tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g* T1_;
	T1_ = (tyObject_Cell__1zcF9cV8XIAtbN8h5HRUB8g*)0;
	T1_ = usrToCell__system_5291(p);
	decRef__system_5336(T1_);
}
N_LIB_PRIVATE N_NIMCALL(void, copyHalf__vm_10880)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* h, tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* result) {
	{
		NI j;
		NI i;
		j = (NI)0;
		i = ((NI) 0);
		{
			while (1) {
				NimStringDesc* T4_;
				if (!(i < ((NI) 256))) goto LA3;
				j = i;
				T4_ = NIM_NIL;
				T4_ = (*result).keys[(j)- 0]; (*result).keys[(j)- 0] = copyStringRC1((*h).keys[((NI)(((NI) 256) + j))- 0]);
				if (T4_) nimGCunrefNoCycle(T4_);
				i += ((NI) 1);
			} LA3: ;
		}
	}
	{
		if (!(*h).isInternal) goto LA7_;
		{
			NI j_2;
			NI i_2;
			j_2 = (NI)0;
			i_2 = ((NI) 0);
			{
				while (1) {
					if (!(i_2 < ((NI) 256))) goto LA11;
					j_2 = i_2;
					asgnRef((void**) (&(*result)._isInternal_2.links[(j_2)- 0]), (*h)._isInternal_2.links[((NI)(((NI) 256) + j_2))- 0]);
					i_2 += ((NI) 1);
				} LA11: ;
			}
		}
	}
	goto LA5_;
	LA7_: ;
	{
		{
			NI j_3;
			NI i_3;
			j_3 = (NI)0;
			i_3 = ((NI) 0);
			{
				while (1) {
					if (!(i_3 < ((NI) 256))) goto LA15;
					j_3 = i_3;
					asgnRef((void**) (&(*result)._isInternal_1.vals[(j_3)- 0]), (*h)._isInternal_1.vals[((NI)(((NI) 256) + j_3))- 0]);
					i_3 += ((NI) 1);
				} LA15: ;
			}
		}
	}
	LA5_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*, split__vm_10863)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* h) {
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* result;
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* T1_;
	result = NIM_NIL;
	T1_ = NIM_NIL;
	T1_ = (tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) newObj((&NTInode__tPE9ckDbaXj82MdUy0JgA7w_), sizeof(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g));
	(*T1_).entries = ((NI) 256);
	(*T1_).isInternal = (*h).isInternal;
	result = T1_;
	(*h).entries = ((NI) 256);
	copyHalf__vm_10880(h, result);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*, insert__vm_10029)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* h, NimStringDesc* key, tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* val) {
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* result;
	NimStringDesc* newKey;
	NI j;
	NimStringDesc* T41_;
{	result = NIM_NIL;
	newKey = copyString(key);
	j = ((NI) 0);
	{
		if (!!((*h).isInternal)) goto LA3_;
		{
			while (1) {
				if (!(j < (*h).entries)) goto LA6;
				{
					NI T9_;
					T9_ = (NI)0;
					T9_ = cmp__system_1549(key, (*h).keys[(j)- 0]);
					if (!(T9_ == ((NI) 0))) goto LA10_;
					asgnRef((void**) (&(*h)._isInternal_1.vals[(j)- 0]), val);
					goto BeforeRet_;
				}
				LA10_: ;
				{
					NI T14_;
					T14_ = (NI)0;
					T14_ = cmp__system_1549(key, (*h).keys[(j)- 0]);
					if (!(T14_ < ((NI) 0))) goto LA15_;
					goto LA5;
				}
				LA15_: ;
				j += ((NI) 1);
			} LA6: ;
		} LA5: ;
		{
			NI i;
			NI colontmp_;
			NI res;
			i = (NI)0;
			colontmp_ = (NI)0;
			colontmp_ = (NI)(j + ((NI) 1));
			res = (*h).entries;
			{
				while (1) {
					if (!(colontmp_ <= res)) goto LA19;
					i = res;
					asgnRef((void**) (&(*h)._isInternal_1.vals[(i)- 0]), (*h)._isInternal_1.vals[((NI)(i - ((NI) 1)))- 0]);
					res -= ((NI) 1);
				} LA19: ;
			}
		}
		asgnRef((void**) (&(*h)._isInternal_1.vals[(j)- 0]), val);
	}
	goto LA1_;
	LA3_: ;
	{
		tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* newLink;
		newLink = ((tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) NIM_NIL);
		{
			while (1) {
				if (!(j < (*h).entries)) goto LA22;
				{
					NIM_BOOL T25_;
					NI T27_;
					tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* u;
					T25_ = (NIM_BOOL)0;
					T25_ = ((NI)(j + ((NI) 1)) == (*h).entries);
					if (T25_) goto LA26_;
					T27_ = (NI)0;
					T27_ = cmp__system_1549(key, (*h).keys[((NI)(j + ((NI) 1)))- 0]);
					T25_ = (T27_ < ((NI) 0));
					LA26_: ;
					if (!T25_) goto LA28_;
					u = insert__vm_10029((*h)._isInternal_2.links[(j)- 0], key, val);
					j += ((NI) 1);
					{
						if (!(u == ((tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) NIM_NIL))) goto LA32_;
						result = ((tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) NIM_NIL);
						goto BeforeRet_;
					}
					LA32_: ;
					newKey = copyString((*u).keys[(((NI) 0))- 0]);
					newLink = u;
					goto LA21;
				}
				LA28_: ;
				j += ((NI) 1);
			} LA22: ;
		} LA21: ;
		{
			NI i_2;
			NI colontmp__2;
			NI res_2;
			i_2 = (NI)0;
			colontmp__2 = (NI)0;
			colontmp__2 = (NI)(j + ((NI) 1));
			res_2 = (*h).entries;
			{
				while (1) {
					if (!(colontmp__2 <= res_2)) goto LA36;
					i_2 = res_2;
					asgnRef((void**) (&(*h)._isInternal_2.links[(i_2)- 0]), (*h)._isInternal_2.links[((NI)(i_2 - ((NI) 1)))- 0]);
					res_2 -= ((NI) 1);
				} LA36: ;
			}
		}
		asgnRef((void**) (&(*h)._isInternal_2.links[(j)- 0]), newLink);
	}
	LA1_: ;
	{
		NI i_3;
		NI colontmp__3;
		NI res_3;
		i_3 = (NI)0;
		colontmp__3 = (NI)0;
		colontmp__3 = (NI)(j + ((NI) 1));
		res_3 = (*h).entries;
		{
			while (1) {
				NimStringDesc* T40_;
				if (!(colontmp__3 <= res_3)) goto LA39;
				i_3 = res_3;
				T40_ = NIM_NIL;
				T40_ = (*h).keys[(i_3)- 0]; (*h).keys[(i_3)- 0] = copyStringRC1((*h).keys[((NI)(i_3 - ((NI) 1)))- 0]);
				if (T40_) nimGCunrefNoCycle(T40_);
				res_3 -= ((NI) 1);
			} LA39: ;
		}
	}
	T41_ = NIM_NIL;
	T41_ = (*h).keys[(j)- 0]; (*h).keys[(j)- 0] = copyStringRC1(newKey);
	if (T41_) nimGCunrefNoCycle(T41_);
	(*h).entries += ((NI) 1);
	{
		if (!((*h).entries < ((NI) 512))) goto LA44_;
		result = ((tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) NIM_NIL);
	}
	goto LA42_;
	LA44_: ;
	{
		result = split__vm_10863(h);
	}
	LA42_: ;
	goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, add__vm_10013)(tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg* b, NimStringDesc* key, tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* val) {
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* u;
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* t;
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* T5_;
	NimStringDesc* T6_;
	NimStringDesc* T7_;
{	u = insert__vm_10029((*b).root, key, val);
	(*b).entries += ((NI) 1);
	{
		if (!(u == ((tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) NIM_NIL))) goto LA3_;
		goto BeforeRet_;
	}
	LA3_: ;
	T5_ = NIM_NIL;
	T5_ = (tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g*) newObj((&NTInode__tPE9ckDbaXj82MdUy0JgA7w_), sizeof(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g));
	(*T5_).entries = ((NI) 2);
	(*T5_).isInternal = NIM_TRUE;
	t = T5_;
	T6_ = NIM_NIL;
	T6_ = (*t).keys[(((NI) 0))- 0]; (*t).keys[(((NI) 0))- 0] = copyStringRC1((*(*b).root).keys[(((NI) 0))- 0]);
	if (T6_) nimGCunrefNoCycle(T6_);
	asgnRef((void**) (&(*t)._isInternal_2.links[(((NI) 0))- 0]), (*b).root);
	T7_ = NIM_NIL;
	T7_ = (*t).keys[(((NI) 1))- 0]; (*t).keys[(((NI) 1))- 0] = copyStringRC1((*u).keys[(((NI) 0))- 0]);
	if (T7_) nimGCunrefNoCycle(T7_);
	asgnRef((void**) (&(*t)._isInternal_2.links[(((NI) 1))- 0]), u);
	unsureAsgnRef((void**) (&(*b).root), t);
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw*, getOrDefault__vm_12020)(tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg b, NimStringDesc* key) {
	tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* result;
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* x;
{	result = NIM_NIL;
	x = b.root;
	{
		while (1) {
			if (!(*x).isInternal) goto LA2;
			{
				NI j;
				NI i;
				j = (NI)0;
				i = ((NI) 0);
				{
					while (1) {
						if (!(i < (*x).entries)) goto LA5;
						j = i;
						{
							NIM_BOOL T8_;
							NI T10_;
							T8_ = (NIM_BOOL)0;
							T8_ = ((NI)(j + ((NI) 1)) == (*x).entries);
							if (T8_) goto LA9_;
							T10_ = (NI)0;
							T10_ = cmp__system_1549(key, (*x).keys[((NI)(j + ((NI) 1)))- 0]);
							T8_ = (T10_ < ((NI) 0));
							LA9_: ;
							if (!T8_) goto LA11_;
							x = (*x)._isInternal_2.links[(j)- 0];
							goto LA3;
						}
						LA11_: ;
						i += ((NI) 1);
					} LA5: ;
				}
			} LA3: ;
		} LA2: ;
	}
	{
		NI j_2;
		NI i_2;
		j_2 = (NI)0;
		i_2 = ((NI) 0);
		{
			while (1) {
				if (!(i_2 < (*x).entries)) goto LA15;
				j_2 = i_2;
				{
					NI T18_;
					T18_ = (NI)0;
					T18_ = cmp__system_1549(key, (*x).keys[(j_2)- 0]);
					if (!(T18_ == ((NI) 0))) goto LA19_;
					result = (*x)._isInternal_1.vals[(j_2)- 0];
					goto BeforeRet_;
				}
				LA19_: ;
				i_2 += ((NI) 1);
			} LA15: ;
		}
	}
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, hasNext__vm_12393)(tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg b, NI index) {
	NIM_BOOL result;
	result = (NIM_BOOL)0;
	result = (index < b.entries);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, countSubTree__vm_12621)(tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* it) {
	NI result;
	result = (NI)0;
	{
		if (!(*it).isInternal) goto LA3_;
		result = ((NI) 0);
		{
			NI k;
			NI i;
			k = (NI)0;
			i = ((NI) 0);
			{
				while (1) {
					NI T8_;
					if (!(i < (*it).entries)) goto LA7;
					k = i;
					T8_ = (NI)0;
					T8_ = countSubTree__vm_12621((*it)._isInternal_2.links[(k)- 0]);
					result += T8_;
					i += ((NI) 1);
				} LA7: ;
			}
		}
	}
	goto LA1_;
	LA3_: ;
	{
		result = (*it).entries;
	}
	LA1_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, next__vm_12544)(tyObject_BTree__VZdzO0Tlflp7WMN4gS8oPg b, NI index, tyTuple__9aIi6GdTSD27YtPkWxMqNxA* Result) {
	tyObject_NodecolonObjectType___dNELmBSmY7nthjhZupWO6g* it;
	NI i;
	NimStringDesc* colontmp_;
	tyObject_TNode__bROa11lyF5vxEN9aYNbHmhw* colontmp__2;
	NI colontmp__3;
	it = b.root;
	i = index;
	{
		while (1) {
			NI sum;
			if (!(*it).isInternal) goto LA2;
			sum = ((NI) 0);
			{
				NI k;
				NI i_2;
				k = (NI)0;
				i_2 = ((NI) 0);
				{
					while (1) {
						NI c;
						if (!(i_2 < (*it).entries)) goto LA5;
						k = i_2;
						c = countSubTree__vm_12621((*it)._isInternal_2.links[(k)- 0]);
						sum += c;
						{
							if (!(i < sum)) goto LA8_;
							it = (*it)._isInternal_2.links[(k)- 0];
							i -= (NI)(sum - c);
							goto LA3;
						}
						LA8_: ;
						i_2 += ((NI) 1);
					} LA5: ;
				}
			} LA3: ;
		} LA2: ;
	}
	colontmp_ = copyString((*it).keys[(i)- 0]);
	colontmp__2 = (*it)._isInternal_1.vals[(i)- 0];
	colontmp__3 = (NI)(index + ((NI) 1));
	unsureAsgnRef((void**) (&(*Result).Field0), copyString(colontmp_));
	unsureAsgnRef((void**) (&(*Result).Field1), colontmp__2);
	(*Result).Field2 = colontmp__3;
}
